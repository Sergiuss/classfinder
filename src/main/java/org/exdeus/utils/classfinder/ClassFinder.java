package org.exdeus.utils.classfinder;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Set;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Find classes by base package and assignable classes.
 * @author sidorov-s
 */
public final class ClassFinder {
    private static final Logger LOGGER = LoggerFactory.getLogger(ClassFinder.class);
    private static final String JAR = "jar";
    private static final String BUNDLE = "bundle";
    private static final String CLASS = ".class";
    private static final String FILE_PREFIX = "file:";

    private ClassFinder() {
    }

    private static final Set<Class<?>> EMPTY_SET = Collections.emptySet();

    public static Set<Class<?>> find(final Set<String> basePackages, final Set<Class<?>> assignables) {
        return find(basePackages, assignables, EMPTY_SET);
    }

    public static Set<Class<?>> find(final Set<String> basePackages, final Set<Class<?>> assignables, final Set<Class<?>> exclude) {
        final Set<Class<?>> foundClasses = new HashSet<Class<?>>();

        final Set<String> normalizedBasePackages = new HashSet<String>();

        for (final String basePackage : basePackages) {
            normalizedBasePackages.add(basePackage.replace('.', '/'));
        }

        findForBasePackages(normalizedBasePackages, assignables, exclude, foundClasses);

        return foundClasses;
    }

    private static void findForBasePackages(final Set<String> basePackages, final Set<Class<?>> assignables,
                                            final Set<Class<?>> exclude, final Set<Class<?>> foundClasses) {
        try {
            final ClassLoader classLoader = Thread.currentThread().getContextClassLoader();

            final Set<String> lookedResourses = new HashSet<String>();

            for (final String basePackage : basePackages) {
                final Enumeration<URL> resources = classLoader.getResources(basePackage);

                while (resources.hasMoreElements()) {
                    final URL resource = resources.nextElement();

                    if (resource.getProtocol().equals(JAR)) {
                        final String jarFileName = getJarFileName(resource);

                        if (!lookedResourses.contains(jarFileName)) {

                            final JarFile jarFile = getJarFile(jarFileName);

                            lookedResourses.addAll(loadFromJarFile(jarFile, basePackages, assignables, exclude, foundClasses));
                        }
                    } else if (resource.getProtocol().startsWith(BUNDLE)) {

                        // not implemented yet
                        loadFromBundle();
                    } else {

                        if (!lookedResourses.contains(resource.getPath())) {
                            lookedResourses.addAll(loadFromDirectory(resource.getPath(), basePackages, assignables, exclude, foundClasses));
                        }
                    }
                }
            }
        } catch (final Exception e) {
            LOGGER.error("Cannot find classes", e);
        }
    }

    private static Set<String> loadFromJarFile(final JarFile jarFile, final Set<String> basePackages,
                                        final Set<Class<?>> assignables, final Set<Class<?>> exclude,
                                        final Set<Class<?>> foundClasses) throws IOException {
        final Set<String> lookedJars = new HashSet<String>();

        lookedJars.add(jarFile.getName());

        if (jarFile != null) {
            try {
                final Enumeration<JarEntry> jarEntries = jarFile.entries();

                while (jarEntries.hasMoreElements()) {
                    final JarEntry jarEntry = jarEntries.nextElement();

                    final String entryName = jarEntry.getName();

                    if (entryName.endsWith("jar")) {
                        // spring uber jar
                        final JarFile springBootJarFile = SpringBootUtils.getSpringBootJarFile(jarEntry);

                        lookedJars.addAll(loadFromJarFile(springBootJarFile, basePackages, assignables, exclude, foundClasses));
                    } else {

                        tryToLoadClass(entryName, basePackages, assignables, exclude, foundClasses);
                    }
                }
            } finally {
                jarFile.close();
            }
        }

        return lookedJars;
    }

    private static Set<String> loadFromDirectory(final String rootDirectory, final Set<String> basePackages,
                                                 final Set<Class<?>> assignables, final Set<Class<?>> exclude,
                                                 final Set<Class<?>> foundClasses) throws UnsupportedEncodingException {
        final Set<String> lookedDirs = new HashSet<String>();

        lookedDirs.add(rootDirectory);

        final File directory = new File(URLDecoder.decode(rootDirectory, "UTF-8"));

        if (directory.exists()) {
            final File[] files = directory.listFiles();

            for (final File file : files) {
                if (file.isDirectory()) {

                    lookedDirs.addAll(loadFromDirectory(file.getPath(), basePackages, assignables, exclude, foundClasses));
                } else {

                    tryToLoadClass(file.getPath(), basePackages, assignables, exclude, foundClasses);
                }
            }
        } else {
            LOGGER.warn("Cannot find package {}", rootDirectory);
        }

        return lookedDirs;
    }

    private static void loadFromBundle() {
        LOGGER.debug("Try to load bundle... But logic is not implemented yet.");
    }

    private static void tryToLoadClass(final String possibleClass, final Set<String> basePackages,
                                  final Set<Class<?>> assignables, final Set<Class<?>> exclude,
                                  final Set<Class<?>> foundClasses) {
        if (possibleClass.endsWith(CLASS)) {
            final String className = possibleClass.replaceAll("\\\\", "/");

            final String basePackage = inBasePackage(className, basePackages);

            if (basePackage != null) {

                final String classNameFormatted = formatClassName(className, basePackage);

                try {
                    final Class<?> foundClass = Class.forName(classNameFormatted);

                    if (filterFoundClass(foundClass, assignables, exclude)) {
                        foundClasses.add(foundClass);
                    }
                } catch (final ClassNotFoundException e) {
                    LOGGER.warn("Cannot find class {}", classNameFormatted, e);
                }
            }
        }
    }

    private static boolean filterFoundClass(final Class<?> foundClass, final Set<Class<?>> assignables, final Set<Class<?>> exclude) {
        if (!exclude.contains(foundClass)) {
            for (final Class<?> assignable : assignables) {
                if (assignable.isAssignableFrom(foundClass)) {
                    return true;
                }
            }
        }

        return false;
    }

    private static String formatClassName(final String classFullName, final String basePackage) {
        return classFullName.substring(classFullName.indexOf(basePackage), classFullName.length() - CLASS.length()).replaceAll("/", ".");
    }

    private static String inBasePackage(final String entryName, final Set<String> basePackages) {
        for (final String basePackage : basePackages) {
            if (entryName.contains(basePackage)) {
                return basePackage;
            }
        }

        return null;
    }

    private static String getJarFileName(final URL jarUrl) throws UnsupportedEncodingException {
        final String jarPath = URLDecoder.decode(jarUrl.getFile(), "UTF-8");

        if (jarPath.endsWith("jar")) {
            return jarPath.substring(FILE_PREFIX.length(), jarPath.length());
        } else {
            return jarPath.substring(FILE_PREFIX.length(), jarPath.indexOf("!"));
        }
    }

    private static JarFile getJarFile(final String entryName) throws IOException {
        if (SpringBootUtils.isSpringBootApplication()) {
            try {
                final File file = new File(entryName);

                final JarFile jarFile = SpringBootUtils.getSpringBootJarFile(file);

                if (jarFile != null) {
                    return jarFile;
                }
            } catch (final Exception e) {
                // nothing
            }
        }

        return new JarFile(entryName);
    }
}

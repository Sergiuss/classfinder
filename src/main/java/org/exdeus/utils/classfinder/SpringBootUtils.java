package org.exdeus.utils.classfinder;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;



/**
 * Containing utilities for spring boot "uber jar".
 * @author sidorov-s
 */
public final class SpringBootUtils {
    private SpringBootUtils() {}

    private static final Class<?> SPRING_BOOT_JAR_FILE_CLASS = getClassByName("org.springframework.boot.loader.jar.JarFile");
    private static final Class<?> SPRING_BOOT_JAR_ENTRY_CLASS = getClassByName("org.springframework.boot.loader.jar.JarEntry");

    public static JarFile getSpringBootJarFile(final Object constructorArg) {
        if (SPRING_BOOT_JAR_FILE_CLASS != null) {
            try {
                final Constructor<?> jarFileConstructor = SPRING_BOOT_JAR_FILE_CLASS.getDeclaredConstructor(constructorArg.getClass());
                jarFileConstructor.setAccessible(true);

                return (JarFile) jarFileConstructor.newInstance(constructorArg);
            } catch (final Exception e) {
                // not a spring boot application
            }
        }

        return null;
    }

    public static JarFile getSpringBootJarFile(final JarEntry jarEntry) {
        if (isSpringBootJarEntry(jarEntry)) {
            try {
                Method method = jarEntry.getClass().getMethod("getSource");

                final Object jarEntryData = method.invoke(jarEntry);

                method = jarEntryData.getClass().getDeclaredMethod("getData");

                final Object randomAccessData = method.invoke(jarEntryData);

                return getSpringBootJarFile(randomAccessData);
            } catch (final Exception e) {
                // not a spring boot application
            }
        }

        return null;
    }

    public static boolean isSpringBootJarEntry(final JarEntry jarEntry) {
        return SPRING_BOOT_JAR_ENTRY_CLASS != null && jarEntry.getClass().isAssignableFrom(SPRING_BOOT_JAR_ENTRY_CLASS);
    }

    public static boolean isSpringBootJarFile(final JarFile jarFile) {
        return SPRING_BOOT_JAR_FILE_CLASS != null && jarFile.getClass().isAssignableFrom(SPRING_BOOT_JAR_FILE_CLASS);
    }

    public static boolean isSpringBootApplication() {
        return SPRING_BOOT_JAR_FILE_CLASS != null;
    }

    private static Class<?> getClassByName(final String className) {
        try {
            return Class.forName(className);
        } catch (final ClassNotFoundException e) {
            return null;
        }
    }
}
